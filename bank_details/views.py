from django.shortcuts import render
from django.http import HttpResponse
import json
import traceback
import sys
from django.core.exceptions import ObjectDoesNotExist

from models import *
# Create your views here.

'''
# status_code == HTTP Status code : 200:OK, 400:Bad Request, 405:Method Not Allowed, 500:Server Error...
# data = response data
# err_code = 0: No Error, 1: Error
'''
def response(http_code, data, status_code):
    response = {
        'responseHeader': {
            'code': status_code
        },
        'response': data
    }
    return HttpResponse(json.dumps(response), content_type='application/json', status=http_code)


'''
# status_code == HTTP Status code : 200:OK, 400:Bad Request, 405:Method Not Allowed, 500:Server Error...
# data = response data
# err_code = 0: No Error, 1: Error
#
'''
def err_response(http_code, err_msg, status_code):
    """

    :rtype : object
    """
    response = {
        'responseHeader': {
            'code': status_code,
            'message': err_msg
        },
        'response': {}
    }
    return HttpResponse(json.dumps(response), content_type='application/json', status=http_code)




def default_view(request):
    return response(200, "Hello there! check out the apis 'api/branch-details-by-ifsc' and 'api/all-branches-in-city'", 0)

def branch_details(request):
    try:
        if request.method == 'GET':
            args = request.GET.dict()
            ifsc = args.get('ifsc').upper()
            if not ifsc: 
                return err_response(400, 'Mandatory params missing.', 1)
            try:
                branch = Branches.objects.get(ifsc=ifsc)
            except ObjectDoesNotExist:
                return err_response(400, "Bank with IFSC %s is not found." %ifsc, 2)
            branch_obj = {
                "ifsc": branch.ifsc,
                "bank": branch.bank.name,
                "branch": branch.branch,
                "address": branch.address,
                "city": branch.city,
                "district": branch.district,
                "state": branch.state
            }
            return response(200, branch_obj, 0)
        else:
            return err_response(405, 'Method Not Allowed', 3)
    except Exception, ex:
        print sys.exc_info()
        traceback.print_exc()
        return err_response(500, "Unexpected server error", 5)




def all_branches_in_city(request):
    try:
        if request.method == 'GET':
            args = request.GET.dict()
            bank = args.get('bank').upper()
            city = args.get('city').upper()
            
            offset = int(args.get('offset', 0))
            limit = int(args.get('limit', 10))

            if not city: 
                return err_response(400, 'Mandatory params missing.', 1)
            
            banks = Branches.objects.filter(city=city)
            
            if bank:
            	banks = banks.filter(bank__name=bank)

            if not banks:
                return err_response(400, "No branches found for %s Bank in %s" %(bank, city), 2)
            
            banks_list = []
            for branch in banks[offset:offset+limit]:
                branch_obj = {
                    "ifsc": branch.ifsc,
                    "bank": branch.bank.name,
                    "branch": branch.branch,
                    "address": branch.address,
                    "city": branch.city,
                    "district": branch.district,
                    "state": branch.state
                }
                banks_list.append(branch_obj)
            output = {
                "branches": banks_list,
                "total": len(Branches.objects.filter(bank__name=bank, city=city))
            }
            return response(200, output, 0)
        else:
            return err_response(405, 'Method Not Allowed', 3)
    except Exception, ex:
        print sys.exc_info()
        traceback.print_exc()
        return err_response(500, "Unexpected server error", 5)