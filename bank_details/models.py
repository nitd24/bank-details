from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Banks(models.Model):
	id = models.BigIntegerField(primary_key=True)
	name = models.CharField(null=True, blank=True, max_length=500)

	class Meta:
		db_table='banks'
		#managed=False



class Branches(models.Model):
	ifsc = models.CharField(null=False, blank=False, max_length=500, primary_key=True)
	bank = models.ForeignKey(Banks)
	branch = models.CharField(null=True, blank=True, max_length=100)
	address = models.TextField()
	city = models.CharField(max_length=100, null=True, blank=True)
	district = models.CharField(max_length=100, null=True, blank=True)
	state = models.CharField(max_length=100, null=True, blank=True)

	class Meta:
		db_table='branches'
		#managed=False


# class BankBranches(models.Model):
# 	ifsc = models.CharField(null=False, blank=False, max_length=500, primary_key=True)
# 	bank = models.ForeignKey(Banks)
# 	branch = models.CharField(null=True, blank=True, max_length=100)
# 	address = models.TextField()
# 	city = models.CharField(max_length=100, null=True, blank=True)
# 	district = models.CharField(max_length=100, null=True, blank=True)
# 	state = models.CharField(max_length=100, null=True, blank=True)

# 	class Meta:
# 		db_table='bank_branches'
# 		#managed=False



