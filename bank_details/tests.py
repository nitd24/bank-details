from django.test import TestCase

import urllib2
import requests
import json

URL = 'https://bank-details-nitish.herokuapp.com/api/'

# Create your tests here.

class SimpleTest(TestCase):

    def test_branch_details(self):
        api_name = 'branch-details-by-ifsc?ifsc=HDFC0001673'
        r = requests.get(URL + api_name)
        self.assertEqual(r.status_code, 200)
        res = json.loads(r.text)
        self.assertIsNotNone(res.get('response'))


    def test_all_branches_in_city(self):
        api_name = 'all-branches-in-city?city=bangalore&bank=hdfc%20BANK&offset=0&limit=20'
        r = requests.get(URL + api_name)
        self.assertEqual(r.status_code, 200)
        res = json.loads(r.text)
        self.assertIsNotNone(res.get('response'))
        self.assertIsNotNone(res.get('response').get('branches'))
        self.assertIsNotNone(res.get('response').get('total'))

        self.assertTrue(len(res.get('response').get('branches')) > 0 )